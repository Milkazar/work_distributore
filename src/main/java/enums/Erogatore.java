package enums;

public enum Erogatore {
    EROGAZIONE_A_CADUTA("A"),
    EROGAZIONE_A_SPINA("B"),
    EROGAZIONE_A_BRACCIO("C"),
    EROGAZIONE_PIATTI_PRONTI("D");


    private final String prefixCode;

    Erogatore(String prefixCode) {
        this.prefixCode = prefixCode;
    }

    public String getPrefixCode() {
        return prefixCode;
    }
}
