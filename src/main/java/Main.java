import exceptions.CreditoInsufficienteException;
import lombok.AllArgsConstructor;
import lombok.Data;
import model.*;
import model.abs.Product;

import java.text.NumberFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    enum OPZIONI {INSERISCI, PRODOTTI, SELEZIONA, RESTO, EXIT, HELP, I, P, S, R, E, H}

    private final NumberFormat df = NumberFormat.getCurrencyInstance();

    private final Map<String, VendingHelper> vendingProducts = new TreeMap<>();
    private double credit;

    public static void main(String[] args) {
        Main main = new Main();
        main.start();
    }

    void start() {
        creaProdotti();
        printProducts(false);
        printOptions();

        Scanner in = new Scanner(System.in);

        while (true) {
            String inOption = in.next().toUpperCase();
            try {
                OPZIONI opzione = OPZIONI.valueOf(inOption);
                handleInput(opzione, in);
            } catch (IllegalArgumentException e) {
                System.err.println("Opzione " + inOption + " non valida!");
                printOptions();
            }
        }
    }

    String underlineText(String t) {
        return "\033[4;97m" + t + "\033[0m";
    }

    void printOptions() {
        System.out.println("Opzioni di digitazione:");
        System.out.println("\t" + underlineText("I") + "NSERISCI (aggiunta credito);");
        System.out.println("\t" + underlineText("P") + "RODOTTI (visualizzazione prodotti);");
        System.out.println("\t" + underlineText("S") + "ELEZIONA (selezione articolo);");
        System.out.println("\t" + underlineText("R") + "ESTO (restituzione resto);");
        System.out.println("\t" + underlineText("E") + "XIT (termina programma);");
        System.out.println("\t" + underlineText("H") + "ELP (mostra questa schermata);");
        System.out.print("Inserire opzione: ");
    }

    void handleInput(OPZIONI option, Scanner scanner) {
        switch (option) {

            case INSERISCI:
            case I:
                handleInserisci(scanner);
                break;
            case P:
            case PRODOTTI:
                printProducts();
                break;
            case SELEZIONA:
            case S:
                handleSeleziona(scanner);
                break;
            case RESTO:
            case R:
                System.out.println("Restituzione resto di " + formatPrice(credit));
                credit = 0d;
                break;
            case EXIT:
            case E:

                System.exit(0);
                break;
            case HELP:
            case H:
                printOptions();
                break;
        }
    }

    private void handleSeleziona(Scanner scanner) {
        System.out.println("Inserire il codice del prodotto desiderato (o CANCEL per annullare): ");
        String codice = scanner.next();

        if ("CANCEL".equalsIgnoreCase(codice)) {
            printOptions();
            return;
        }

        VendingHelper helper = vendingProducts.get(codice.trim().toUpperCase());
        if (helper == null) {
            System.err.println("Codice inserito inesistente.");
            handleSeleziona(scanner);
            return;
        }
        List<Product> products = helper.getProducts();
        if (products.isEmpty()) {
            System.err.println("Prodotto esaurito.");
            handleSeleziona(scanner);
            return;
        }

        Product product = products.remove(0);
        try {
            product.seleziona(credit);
            credit = credit - product.getPrice();
            printProducts();
        } catch (CreditoInsufficienteException e) {
            System.err.println("Credito insufficiente!");
            printProducts();
            handleSeleziona(scanner);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void handleInserisci(Scanner scanner) {
        System.out.println("Quanto credito vuoi inserire? (es 0.50) - digitare CANCEL per annullare");
        String newCredit = scanner.next();
        if ("CANCEL".equalsIgnoreCase(newCredit)) {
            printOptions();
            return;
        }
        try {
            credit = credit + Double.parseDouble(newCredit);
            System.out.println("******CREDITO AGGIUNTO*******");
            System.out.println("******NUOVO CREDITO " + formatPrice(credit) + "*******");
            printOptions();
        } catch (NumberFormatException e) {
            System.err.println("Formato di " + newCredit + " non valido.");
            handleInserisci(scanner);
        }
    }

    String formatPrice(double val) {
        return df.format(val);
    }

    void printProducts() {
        printProducts(true);
    }

    void printProducts(boolean showInserisci) {

        vendingProducts.forEach((key, value) -> {

            StringBuilder sb = new StringBuilder();
            sb.append(key)
                    .append(": ")
                    .append(formatPrice(value.getPrice()))
                    .append(" - ")
                    .append(value.getName());

            if (value.getProducts().isEmpty()) {
                sb.append(" (esaurito)");
            } else {
                sb.append(" (")
                        .append(value.getProducts().size())
                        .append(")");
            }
            System.out.println(sb);
        });
        System.out.println("### Credito: " + formatPrice(credit) + " ###");

        if (showInserisci) {
            System.out.print("Inserire opzione: ");
        }
    }

    private void creaProdotti() {
        List<ProductCreationHelper> productHelpers = new ArrayList<>();
        productHelpers.add(new ProductCreationHelper(AcquaFrizzante.class, 1d));
        productHelpers.add(new ProductCreationHelper(AcquaNaturalePlastica.class, 1d));
        productHelpers.add(new ProductCreationHelper(AcquaNaturaleVetro.class, 1.3d));
        productHelpers.add(new ProductCreationHelper(Caffe.class, 0.8d));
        productHelpers.add(new ProductCreationHelper(CocaCola.class, 1.5d));
        productHelpers.add(new ProductCreationHelper(CocaColaSpina.class, 1.2d));
        productHelpers.add(new ProductCreationHelper(Fanta.class, 1.5d));
        productHelpers.add(new ProductCreationHelper(FantaSpina.class, 1.2d));
        productHelpers.add(new ProductCreationHelper(InsalataDiRiso.class, 4.5d));
        productHelpers.add(new ProductCreationHelper(Lasagne.class, 5d));
        productHelpers.add(new ProductCreationHelper(Schiacciatine.class, 2.3d));

        Random rand = new Random();

        Map<String, Integer> mapCounter = new HashMap<>();

        for (ProductCreationHelper model : productHelpers) {
            int total = rand.nextInt(1) + 2;

            List<Product> products = IntStream.rangeClosed(0, total).mapToObj(index -> {
                try {
                    return model.create();
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                return null;
            }).collect(Collectors.toList());
            Product specimen = products.get(0);
            String prefix = specimen.getErogazione().getPrefixCode();

            int counter = mapCounter.getOrDefault(prefix, 0);
            counter++;
            mapCounter.put(prefix, counter);
            String code = prefix + counter;
            products.forEach(p -> p.setCodice(code));
            vendingProducts.put(code, new VendingHelper(products, specimen.getName(), specimen.getPrice()));

        }
    }

    @Data
    @AllArgsConstructor
    public static class VendingHelper {
        private List<Product> products;
        private String name;
        private double price;
    }
}
