package model.abs;

import enums.Erogatore;
import model.interfaces.Specifiable;

public abstract class ProductPiattoPronto extends Product implements Specifiable {

    {
        erogazione = Erogatore.EROGAZIONE_PIATTI_PRONTI;
    }

    public ProductPiattoPronto(String name) {
        super(name);
    }

    public ProductPiattoPronto(String name, String codice) {
        super(name, codice);
    }

    @Override
    protected void consegnaProdotto() {
        System.out.println("Apertura sportello.");
        System.out.println("Erogazione piatto pronto completato. E' possibile prelevare '" + name + "'");
    }

    @Override
    protected void stampaCaratteristiche() {
        System.out.println("un generico piatto pronto di tipo " + getClass().getSimpleName());
    }

    @Override
    protected void procedimento() throws InterruptedException {
        initProcedimento();
        procedimentoSpecifico();
        postProcedimento();
    }

}
