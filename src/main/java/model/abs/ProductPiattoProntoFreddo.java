package model.abs;

public abstract class ProductPiattoProntoFreddo extends ProductPiattoPronto {

    public ProductPiattoProntoFreddo(String name) {
        super(name);
    }

    public ProductPiattoProntoFreddo(String name, String codice) {
        super(name, codice);
    }


    @Override
    public void procedimentoSpecifico() {

    }

    @Override
    public void postProcedimento() {

    }

    @Override
    public void initProcedimento() throws InterruptedException {
        System.out.println("Prelevamento dal frigo interno in corso...");
        doTimeout(2);
    }

}
