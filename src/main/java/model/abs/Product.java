package model.abs;

import enums.Erogatore;
import exceptions.CreditoInsufficienteException;
import lombok.Data;

@Data
public abstract class Product {

    protected double price;
    protected final String name;
    protected Erogatore erogazione;
    protected String codice;

    public Product(String name) {
        this.name = name;
    }

    public Product(String name, String codice) {
        this(name);
        this.codice = codice;
    }

    public void seleziona(double credito) throws CreditoInsufficienteException, InterruptedException {
        checkCreditoSufficiente(credito);
        eroga();
    }

    private void checkCreditoSufficiente(double credito) throws CreditoInsufficienteException {
        if (credito < price) {
            throw new CreditoInsufficienteException("Credito insufficiente");
        }
    }

    protected abstract void stampaCaratteristiche();

    private void eroga() throws InterruptedException {
        procedimento();
        consegnaProdotto();
    }

    protected abstract void consegnaProdotto();

    protected abstract void procedimento() throws InterruptedException;

    protected void doTimeout(int seconds) throws InterruptedException {
        for (int i = seconds; i > 0; i--) {
            System.out.println("Tempo rimanente: " + i + " secondi");
            Thread.sleep(1000);
        }
    }
}
