package model.abs;

import enums.Erogatore;

public abstract class ProductABraccio extends Product {

    {
        erogazione = Erogatore.EROGAZIONE_A_BRACCIO;
    }

    public ProductABraccio(String name) {
        super(name);
    }

    public ProductABraccio(String name, String codice) {
        super(name, codice);
    }

    @Override
    protected void procedimento() {
        System.out.println("Il braccio si posiziona davanti all'apertura " + codice + ".");
        System.out.println("Il pistone spinge in avanti il prodotto.");
        System.out.println("Il braccio si posiziona davanti al vetro di consegna.");
    }

    @Override
    protected void consegnaProdotto() {
        System.out.println("Il vetro di consegna si apre, è ora possibile prelevare '" + name + "'");
    }


    @Override
    protected void stampaCaratteristiche() {
        System.out.println("un generico prodotto a braccio di tipo " + getClass().getSimpleName());
    }
}
