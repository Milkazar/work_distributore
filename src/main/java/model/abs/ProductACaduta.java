package model.abs;

import enums.Erogatore;

public abstract class ProductACaduta extends Product {

    {
        erogazione = Erogatore.EROGAZIONE_A_CADUTA;
    }

    public ProductACaduta(String name) {
        super(name);
    }

    public ProductACaduta(String name, String codice) {
        super(name, codice);
    }

    @Override
    protected void procedimento() {
        System.out.println("Seleziono la spirale con il codice " + codice + ".");
    }

    @Override
    protected void consegnaProdotto() {
        System.out.println("La spirale gira e il prodotto '" + name + "' cade.");
    }

    @Override
    protected void stampaCaratteristiche() {
        System.out.println("un generico prodotto a caduta di tipo " + getClass().getSimpleName());
    }
}
