package model.abs;

import enums.Erogatore;
import model.interfaces.Specifiable;

public abstract class ProductASpina extends Product implements Specifiable {

    {
        erogazione = Erogatore.EROGAZIONE_A_SPINA;
    }

    public ProductASpina(String name) {
        super(name);
    }

    public ProductASpina(String name, String codice) {
        super(name, codice);
    }

    @Override
    protected void consegnaProdotto() {
        System.out.println("Erogazione a spina completata. E' possibile prelevare '" + name + "'");
    }

    @Override
    protected void procedimento() throws InterruptedException {
        initProcedimento();
        procedimentoSpecifico();
        postProcedimento();
    }

    @Override
    protected void stampaCaratteristiche() {
        System.out.println("un generico prodotto a spina di tipo " + getClass().getSimpleName());
    }


}
