package model.abs;

public abstract class ProductASpinaCaldo extends ProductASpina {

    public ProductASpinaCaldo(String name) {
        super(name);
    }

    public ProductASpinaCaldo(String name, String codice) {
        super(name, codice);
    }

    @Override
    public void initProcedimento() throws InterruptedException {
        System.out.println("Cade il bicchiere dall'alto nel braccetto.");
        System.out.println("Scaldo il liquido...");
        doTimeout(2);
    }

    @Override
    public void postProcedimento() throws InterruptedException {
        System.out.println("Erogazione liquido caldo in corso");
    }
}
