package model.abs;

public abstract class ProductASpinaFreddo extends ProductASpina {

    public ProductASpinaFreddo(String name) {
        super(name);
    }

    public ProductASpinaFreddo(String name, String codice) {
        super(name, codice);
    }

    @Override
    public void procedimentoSpecifico() throws InterruptedException {
        System.out.println("Erogazione polvere nel bicchiere...");
        doTimeout(1);
        System.out.println("Raffreddamento acqua in corso...");
        doTimeout(2);
    }

    @Override
    public void postProcedimento() throws InterruptedException {
        System.out.println("Erogazione acqua fredda in corso");
        doTimeout(2);
    }

    @Override
    public void initProcedimento() throws InterruptedException {
        System.out.println("Cade il bicchiere dall'alto nel braccetto.");
    }
}
