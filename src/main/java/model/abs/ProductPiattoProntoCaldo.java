package model.abs;

public abstract class ProductPiattoProntoCaldo extends ProductPiattoPronto {

    public ProductPiattoProntoCaldo(String name) {
        super(name);
    }

    public ProductPiattoProntoCaldo(String name, String codice) {
        super(name, codice);
    }


    @Override
    public void procedimentoSpecifico() throws InterruptedException {
        System.out.println("Riscaldamento piatto in corso...");
        doTimeout(6);
    }

    @Override
    public void postProcedimento() throws InterruptedException {
        System.out.println("Attesa per raffreddamento (attenzione scotta)..");
        doTimeout(4);
    }

    @Override
    public void initProcedimento() throws InterruptedException {
        System.out.println("Prelevamento dal frigo interno in corso...");
        doTimeout(2);
    }


}
