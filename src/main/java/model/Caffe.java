package model;

import model.abs.ProductASpinaCaldo;

public class Caffe extends ProductASpinaCaldo {
    public Caffe() {
        super("Caffé");
    }

    @Override
    public void procedimentoSpecifico() throws InterruptedException {
        System.out.println("Macino il caffé");
        doTimeout(3);
        System.out.println("Erogazione caffé macinato in corso...");
        doTimeout(2);
    }


}
