package model.interfaces;

public interface Specifiable {
    void procedimentoSpecifico() throws InterruptedException;

    void initProcedimento() throws InterruptedException;

    void postProcedimento() throws InterruptedException;
}
