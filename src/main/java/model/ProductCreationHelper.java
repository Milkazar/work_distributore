package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import model.abs.Product;

@Data
@AllArgsConstructor
public class ProductCreationHelper {

    private Class<?> clazz;
    private double price;


    public Product create() throws InstantiationException, IllegalAccessException {
        Product product = (Product) clazz.newInstance();
        product.setPrice(price);
        return product;
    }
}
